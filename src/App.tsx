import React, {useEffect, useState} from "react";
import shuffle from "shuffle-array";
import Tile from './components/Tile'
import "./Styles/App.scss";
import {dataType} from "./interfaces/interfaces";

const bbb = [
    "*child noises in the background*",
    "Hello, hello?",
    "I need to jump in another call",
    "can everyone go on mute",
    "could you please get closer to the mic",
    "*load painful echo/feedback*",
    "Next slide, please.",
    "can we take this offline?",
    "is ___ on the call?",
    "could you share this slides afterward?",
    "Can somebody grant presenter right?",
    "Can you email that to everyone?",
    "sorry, I had problems logging in",
    "*animal noises in the background*",
    "Sorry I didn't found the conference id",
    "I was having connection issues",
    "I'll have to get back to you",
    "Who just joined?",
    "Sorry, something ___ with my calendar",
    "do you see my screen?",
    "Lets wait for ___!",
    "You will send the minutes?",
    "Sorry, i was on mute",
    "Can you repeat, please?",
];

function App() {
    const [data, setData] = useState<dataType>({});

    useEffect(() => {
        let bingoList = shuffle(bbb);
        bingoList.splice(12, 0, "CONF CALL 😷 BINGO");
        const bingoData = bingoList.reduce(
            (data, value, index) => ({
                ...data, [index]: {
                    text: value,
                    won: false,
                    selected: index === 12
                }
            }),
            {}
        );
        setData({...bingoData});
    }, []);

    const checkWon = (data: dataType) => {
        const range = [0, 1, 2, 3, 4];
        const winningDiagonalCords: Array<Number> = [];

        //diagonal
        range.forEach(i => {
            //top left to bottom right
            if (range.every(i => data[i * 5 + i].selected)) {
                winningDiagonalCords.push(i * 5 + i)
            }
            //top right to bottom left
            if (range.every(i => data[(i + 1) * 4].selected)) {
                winningDiagonalCords.push((i + 1) * 4)
            }
        })

        const winningRows = range.filter((row) => range.every((column) => data[row * 5 + column].selected)) //rows winning
        const winningCols = range.filter((column) => range.every((row) => data[row * 5 + column].selected)) //cols winning
        range.forEach(row => {
            range.forEach(column => {
                const index = row * 5 + column
                data[index].won = winningRows.includes(row) || winningCols.includes(column) || winningDiagonalCords.includes(index)
            })
        })
    };

    const toggle = (id: string) => {
        if (id === "12") return;
        data[id].selected = !data[id].selected
        checkWon(data)
        setData({...data})
    }

    return (
        <div className="App">
            <h1>Bingo</h1>
            <div className="wrapper">
                {Object.keys(data).map((id) => (
                    <Tile
                        key={id}
                        data={data[id]}
                        onToggle={() => toggle(id)}
                    />
                ))}
            </div>
        </div>
    );
}

export default App;
