import {FC, MouseEventHandler} from "react";
import {tileType} from "../interfaces/interfaces";

interface Props {
    data: tileType
    onToggle: MouseEventHandler
}

const Tile: FC<Props> = ({data, onToggle}) => {
    return (
        <div onClick={onToggle} className={`tile ${data.selected ? "tile--set" : ""} ${data.won ? "won" : ""}`}>
            {data.text}
        </div>
    );
}

export default Tile