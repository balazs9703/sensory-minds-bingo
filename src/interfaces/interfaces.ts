export interface dataType {
    [key: string]: tileType
}

export interface tileType {
    selected: Boolean
    text: String
    won: Boolean
}